export const getViewport = () => ({ width: document.documentElement.clientWidth, height: document.documentElement.clientHeight })
export const isVertical = () => getComputedStyle(document.documentElement).writingMode.startsWith('vertical-')
export const isVerticalRL = () => getComputedStyle(document.documentElement).writingMode === 'vertical-rl'
export const inlineStartOf = (box, isRTL) => isVertical() ? box.top : (isRTL ? getViewport().width - box.right : box.left)
export const inlineEndOf = (box, isRTL) => isVertical() ? box.bottom : (isRTL ? getViewport().width - box.left : box.right)
export const blockStartOf = (box) => isVertical() ? (isVerticalRL() ? getViewport().width - box.right : box.left) : box.top
export const blockEndOf = (box) => isVertical() ? (isVerticalRL() ? getViewport().width - box.left : box.right) : box.bottom
export const inlineSizeOf = (box) => isVertical() ? box.height : box.width
export const blockSizeOf = (box) => isVertical() ? box.width : box.height
export const offsetBlockStartOf = (box) => isVertical() ? box.offsetLeft : box.offsetTop
export const offsetBlockEndOf = (box) => isVerticalRL() ? (box.offsetParent?.clientWidth || getViewport().width) - box.offsetLeft : offsetBlockStartOf(box) + offsetBlockSizeOf(box)
export const offsetInlineSizeOf = (box) => isVertical() ? box.offsetHeight : box.offsetWidth
export const offsetBlockSizeOf = (box) => isVertical() ? box.offsetWidth : box.offsetHeight
export const scrollBlockStartMaxSizeOf = (box) => isVertical() ? box.scrollWidth - box.clientWidth : box.scrollHeight - box.clientHeight
export const scrollBlockStartOf = (box) => isVertical() ? box.scrollLeft : box.scrollTop
export const scrollBlockSizeOf = (box) => isVertical() ? box.scrollWidth : box.scrollHeight
